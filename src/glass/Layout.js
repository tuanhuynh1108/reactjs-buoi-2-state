import React, { Component } from "react";
import ImageModel from "./ImageModel";
import "./Layout.css";

export default class Layout extends Component {
  render() {
    return (
      <div>
        <div className="bg-dark text-white px-5 py-5 text-center">
          <p className="h2">TRY GLASSES APP ONLINE</p>
        </div>
        <div className="bg">
          <ImageModel />
        </div>
      </div>
    );
  }
}
