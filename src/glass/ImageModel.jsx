import React, { Component } from 'react'
import './ImageModel.css'

export default class ImageModel extends Component {
  state = {
    imgSrc: './glassesImage/v1.png',
  }
  handleChangeGlasses = (number) => {
    let newImg = `./glassesImage/v${number}.png`
    this.setState({
      imgSrc: newImg,
    })
  }
  render() {
    return (
      <div className="position-relative">
        <img className="position-relative" src="./glassesImage/model.jpg" />
        <img className="position-absolute glass" src={this.state.imgSrc} />
        <div>
          <button>
            <img
              src="./glassesImage/v2.png"
              alt="#"
              onClick={() => {
                this.handleChangeGlasses('2')
              }}
            />
          </button>
          <button>
            <img
              src="./glassesImage/v3.png"
              alt="#"
              onClick={() => {
                this.handleChangeGlasses('3')
              }}
            />
          </button>
          <button>
            <img
              src="./glassesImage/v4.png"
              alt="#"
              onClick={() => {
                this.handleChangeGlasses('4')
              }}
            />
          </button>
          <button>
            <img
              src="./glassesImage/v5.png"
              alt="#"
              onClick={() => {
                this.handleChangeGlasses('5')
              }}
            />
          </button>
          <button>
            <img
              src="./glassesImage/v6.png"
              alt="#"
              onClick={() => {
                this.handleChangeGlasses('6')
              }}
            />
          </button>
          <button>
            <img
              src="./glassesImage/v7.png"
              alt="#"
              onClick={() => {
                this.handleChangeGlasses('7')
              }}
            />
          </button>
          <button>
            <img
              src="./glassesImage/v8.png"
              alt="#"
              onClick={() => {
                this.handleChangeGlasses('8')
              }}
            />
          </button>
          <button>
            <img
              src="./glassesImage/v9.png"
              alt="#"
              onClick={() => {
                this.handleChangeGlasses('9')
              }}
            />
          </button>
        </div>
      </div>
    )
  }
}
